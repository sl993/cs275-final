import java.util.Scanner;
import java.util.ArrayList;

import com.temboo.core.TembooSession;


public class FBDriver {

	
	public static void main(String[] args) throws Exception
	{
		FaceBook fb = new FaceBook();
		
		TembooSession t = new TembooSession("leolab", "myFirstApp", "9f5966ce6e3044f0be4adeb0893b42a5");
		
		fb.OAuth(t);
		
		System.out.print("Oauth Complete!");
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the terms you wish to search for:");
		
		String terms = keyboard.nextLine();
		
		ArrayList<String[]> list = fb.Search(t, terms);
		
		for(int i = 0; i < list.size(); i++)
		{
			System.out.print(list.get(i)[0] + ", " + list.get(i)[1] + "/n");
		}
		
		keyboard.close();
		
	}
	
}
