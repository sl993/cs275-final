Shengyang Lai, Gong Zhang, Noah Nam, Nik Madan
Web and Mobile App Development
Final Project
email: nkn28@drexel.edu

This program searches through the user's Facebook and Twitter accounts for posts that contain a search term.

This program was written in both Eclipse Mars and Android Studio in Windows 7 and 8.

This program does not function as intended.

More details are found in Writeup.pdf.

CS 275 Final contains Nik's Facebook code.
P contains Noah's Twitter code.
Post Searcher contains Shenyang's and Gong's front-end code.