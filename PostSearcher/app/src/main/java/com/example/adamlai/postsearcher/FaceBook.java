package com.example.adamlai.postsearcher;
//import java.io.InputStream;
//import java.io.InputStreamReader;
import android.os.AsyncTask;

import java.util.Scanner;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Facebook.OAuth.FinalizeOAuth;
import com.temboo.Library.Facebook.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Facebook.OAuth.InitializeOAuth;
import com.temboo.Library.Facebook.OAuth.InitializeOAuth.*;
import com.temboo.Library.Facebook.Reading.ProfileFeed;
import com.temboo.Library.Facebook.Reading.ProfileFeed.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class FaceBook {


    public String FBOauth;

    public FaceBook(){

        FBOauth = null;

    }

    public String OAuth(TembooSession session) throws Exception
    {
        // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:

        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        // Get an InputSet object for the choreo
        InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set inputs
        initializeOAuthInputs.set_AppID("463635517142696");
        initializeOAuthInputs.set_ForwardingURL("https://cs.drexel.edu");
        initializeOAuthInputs.set_Scope("user_posts");

        // Execute Choreo
        InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);

        //Scanner in = new Scanner(System.in);

        //System.out.println("Please visit the URL below, follow the instructions, and then press enter.");
        //System.out.println(initializeOAuthResults.get_AuthorizationURL());
        return initializeOAuthResults.get_AuthorizationURL();}
        /*in.nextLine();

        String callback = initializeOAuthResults.get_CallbackID();

        // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
        // TembooSession session = new TembooSession("leolab", "myFirstApp", "9f5966ce6e3044f0be4adeb0893b42a5");

        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_CallbackID(callback);
        finalizeOAuthInputs.set_AppSecret("168a4d8263b91524b2d340251743448a");
        finalizeOAuthInputs.set_AppID("463635517142696");

        // Execute Choreo
        FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);

        FBOauth = finalizeOAuthResults.get_AccessToken();
    }*/

    public ArrayList <String[]> Search (TembooSession session, String terms, String FBOauth) throws Exception
    {

        JsonParser jParse = new JsonParser();
        JsonElement data;
        JsonObject datObj;
        ArrayList <String[]> list = new ArrayList <String[]> ();

        // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
        // TembooSession session = new TembooSession("leolab", "myFirstApp", "9f5966ce6e3044f0be4adeb0893b42a5");

        ProfileFeed statusesChoreo = new ProfileFeed(session);

        // Get an InputSet object for the choreo
        ProfileFeedInputSet statusesInputs = statusesChoreo.newInputSet();

        // Set inputs
        statusesInputs.set_AccessToken(FBOauth);
        statusesInputs.set_Fields("id,message");
        statusesInputs.set_Filter("app_2915120374");

        // Execute Choreo
        ProfileFeedResultSet statusesResults = statusesChoreo.execute(statusesInputs);

        data = jParse.parse(statusesResults.get_Response());

        datObj = data.getAsJsonObject();

        JsonArray jArray = datObj.get("data").getAsJsonArray();

        //Create ArrayList of String Arrays. list[int i][0] is the post ID; list[int i][1] is the content.

        for(int i = 0; i < jArray.size(); i++)
        {
            String text = "";

            try{
                text = jArray.get(i).getAsJsonObject().get("message").getAsString();
                System.out.println("stuff");
            }
            catch(NullPointerException e){
                System.out.println("break");
                break;
            }
            finally{
                if(text.contains(terms))
                {

                    String[] add = new String[2];

                    add[0] = jArray.get(i).getAsJsonObject().get("id").getAsString();

                    add[1] = text;

                    list.add(add);

                    System.out.println("+1!");
                }
            }
        }

        return list;
    }

}
