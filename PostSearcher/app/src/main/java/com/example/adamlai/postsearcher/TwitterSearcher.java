package com.example.adamlai.postsearcher;

/**
 * Created by Adam.Lai on 2015/9/3.
 */

    // Prints the grade level required to understand the tweets of a given Twitter user.
    import android.content.Intent;
    import android.net.Uri;

    import java.io.IOException;
    import java.net.URI;
    import java.net.URISyntaxException;
    import java.util.ArrayList;
    import java.util.Scanner;
    import com.google.gson.JsonArray;
    import com.google.gson.JsonElement;
    import com.google.gson.JsonIOException;
    import com.google.gson.JsonObject;
    import com.google.gson.JsonParser;
    import com.google.gson.JsonSyntaxException;
    import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
    import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
    import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
    import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
    import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
    import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
    import com.temboo.Library.Twitter.Timelines.UserTimeline;
    import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
    import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
    import com.temboo.core.TembooException;
    import com.temboo.core.TembooSession;

    import static android.support.v4.app.ActivityCompat.startActivity;

public class TwitterSearcher {
        static String consumerKey = "rXxSz0QTfgDZpCdc9EvJhmQlG";	//consumer key from twitter API
        static String consumerSecret = "gyfZCxY7kCp19SOHcorbDIe22M9vHtHMCio8oV1C54eiYSBRgi"; //consumer secret from twitter API
        static String accessToken;	//access token for temboo twitter
        static String accessSecret;	//access secret for temboo twitter
        static String searchTerm;
        static String authURLString;
        static String twitterUser;	//will hold name of twitter handle to be processed

        static ArrayList<String[]> returnList = new ArrayList<String[]>();

        public static String getURLString()
        {
            return authURLString;
        }

        //First step of the OAuth process, get the user's permission, and also get the twitter account to process.
        public static InitializeOAuthResultSet requestAuth(TembooSession session) throws TembooException, URISyntaxException, IOException{
            InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

            // Get an InputSet object for the choreo
            InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

            // Set inputs
            initializeOAuthInputs.set_ConsumerSecret(consumerSecret);
            initializeOAuthInputs.set_ConsumerKey(consumerKey);

            // Execute Choreo
            InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);

            authURLString = initializeOAuthResults.get_AuthorizationURL();

            //Uri uriAuth = new Uri.parse(authURLString);

            //System.out.println("Please click 'Authorize app' on the webpage that will appear.");
            //java.awt.Desktop.getDesktop().browse(uriAuth);
            //Intent intent = new Intent(Intent.ACTION_VIEW, uriAuth);
            //startActivity();
            return initializeOAuthResults;
        }

        //Get access tokens to use Temboo Twitter choreos.
        public static void requestToken(TembooSession session, InitializeOAuthResultSet initializeOAuthResults) throws TembooException{
            FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

            // Get an InputSet object for the choreo
            FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

            // Set inputs
            finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
            finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());
            finalizeOAuthInputs.set_ConsumerSecret(consumerSecret);
            finalizeOAuthInputs.set_ConsumerKey(consumerKey);

            // Execute Choreo
            FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);

            accessToken = finalizeOAuthResults.get_AccessToken();
            accessSecret = finalizeOAuthResults.get_AccessTokenSecret();
        }

        //Get 200 tweets from the twitter account and return the JsonArray.
        public static JsonArray getTweets(TembooSession session) throws TembooException{
            UserTimeline userTimelineChoreo = new UserTimeline(session);

            // Get an InputSet object for the choreo
            UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

            // Set inputs
            userTimelineInputs.set_ScreenName(twitterUser);
            userTimelineInputs.set_Count("180");	//Max posts per call allowed by Twitter API
            userTimelineInputs.set_AccessToken(accessToken);
            userTimelineInputs.set_AccessTokenSecret(accessSecret);
            userTimelineInputs.set_ConsumerSecret(consumerSecret);
            userTimelineInputs.set_ConsumerKey(consumerKey);

            if(returnList.size() > 0){
                String s = returnList.get(returnList.size()-1)[2];
                Long t = (Long.parseLong(s));
                t = t - 1;

                userTimelineInputs.set_MaxId(Long.toString(t));
            }
            // Execute Choreo
            UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(userTimelineResults.get_Response());
            JsonArray rootArr = root.getAsJsonArray();

            return rootArr;
        }

        //extract tweets from the JSON we got from the previous method
        public static void extractTweets(JsonArray rootArr){
            String[] tweetArray = new String[rootArr.size()];

            for(int i = 0; i < tweetArray.length; ++i){
                String[] s = new String[3];
                s[0] = "https://twitter.com/" + twitterUser + "/status/" + rootArr.get(i).getAsJsonObject().get("id_str").getAsString();
                s[1] = rootArr.get(i).getAsJsonObject().get("text").getAsString();
                s[2] = rootArr.get(i).getAsJsonObject().get("id_str").getAsString();

                returnList.add(s);
            }
        }

        public static ArrayList<String[]> search(String s, String u) throws TembooException, URISyntaxException, IOException{
            TembooSession session = new TembooSession("gisun", "myFirstApp", "0e0ed551d2c04bae9bd5a3c9f8b58242");

            searchTerm = s;
            twitterUser = u;

            InitializeOAuthResultSet initializeOAuthResults = requestAuth(session);

            requestToken(session, initializeOAuthResults);


            System.out.println(accessToken);
            System.out.println(accessSecret);

            JsonArray rootArr = getTweets(session);
            extractTweets(rootArr);

            while(!(rootArr.size() == 0)){
                rootArr = getTweets(session);
                extractTweets(rootArr);
            }


            for(int i = 0; i<returnList.size();){
                if (!returnList.get(i)[1].contains(searchTerm)){
                    returnList.remove(i);
                    //--i;
                }
                else
                    ++i;
            }

            /*for(int i = 0; i < returnList.size(); ++i){
                System.out.println(returnList.get(i)[0]);
                System.out.println(returnList.get(i)[1]);
            }*/
            //System.out.println(returnList.size());
            return returnList;
        }
    }


