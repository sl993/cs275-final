// Prints the grade level required to understand the tweets of a given Twitter user.
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.Library.Wordnik.Word.GetHyphenation;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationInputSet;
import com.temboo.Library.Wordnik.Word.GetHyphenation.GetHyphenationResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class TwitterLevelChecker {
	static String consumerKey = "3YBkBvshPiCrCNt3UEKBKRBFI";	//consumer key from twitter API
	static String consumerSecret = "AOffRodvfKvzmViyaB2RxctrOLVtfmGgP3wSBAheLkFr5LCuLo"; //consumer secret from twitter API
	static String accessToken;	//access token for temboo twitter
	static String accessSecret;	//access secret for temboo twitter
	
	static String twitterUser;	//will hold name of twitter handle to be processed
	
	static String wordnikKey = "a467126718b9ea2dec4030e33a30c48601a886ab2b37d587c";	//API key for wordnik
	
	//First step of the OAuth process, get the user's permission, and also get the twitter account to process.
	public static InitializeOAuthResultSet requestAuth(TembooSession session) throws TembooException, URISyntaxException, IOException{
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ConsumerSecret(consumerSecret);
		initializeOAuthInputs.set_ConsumerKey(consumerKey);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String authURLString = initializeOAuthResults.get_AuthorizationURL();
		
		URI uriAuth = new URI(authURLString);
		
		System.out.println("Please click 'Authorize app' on the webpage that will appear.");
		java.awt.Desktop.getDesktop().browse(uriAuth);
		
		System.out.println("When finished, please enter the username of the Twitter "
				+ "account you wish to process: ");
		Scanner in = new Scanner(System.in);
		twitterUser = in.nextLine();
		in.close();
		
		return initializeOAuthResults;
	}
	
	//Get access tokens to use Temboo Twitter choreos.
	public static void requestToken(TembooSession session, InitializeOAuthResultSet initializeOAuthResults) throws TembooException{
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
		finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());
		finalizeOAuthInputs.set_ConsumerSecret(consumerSecret);
		finalizeOAuthInputs.set_ConsumerKey(consumerKey);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		accessToken = finalizeOAuthResults.get_AccessToken();
		accessSecret = finalizeOAuthResults.get_AccessTokenSecret();
	}
	
	//Get 30 tweets from the twitter account and return the JsonArray.
	public static JsonArray getTweets(TembooSession session) throws TembooException{
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(twitterUser);
		userTimelineInputs.set_Count("30");
		userTimelineInputs.set_AccessToken(accessToken);
		userTimelineInputs.set_AccessTokenSecret(accessSecret);
		userTimelineInputs.set_ConsumerSecret(consumerSecret);
		userTimelineInputs.set_ConsumerKey(consumerKey);

		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(userTimelineResults.get_Response());
		JsonArray rootArr = root.getAsJsonArray();
		
		return rootArr;
	}
	
	//extract tweets from the JSON we got from the previous method
	public static String[] extractTweets(JsonArray rootArr){
		String[] tweetArray = new String[rootArr.size()];
		
		String temp;
		
		for(int i = 0; i < tweetArray.length; ++i){
			temp = rootArr.get(i).getAsJsonObject().get("text").getAsString();
			tweetArray[i] = temp;
		}
		
		return tweetArray;
	}
	
	//split a given tweet into individual words
	public static String[] splitTweet(String tweet){
		tweet = tweet.toLowerCase();
		tweet = tweet.replaceAll("[^a-z0-9 ]","");
		
		String[] wordArray;
		
		wordArray = tweet.split("[ ]+");
		
		return wordArray;
	}
	
	//Check if a word has greater than 3 syllables
	public static boolean isPolysyllabic(TembooSession session, String word) throws JsonIOException, JsonSyntaxException, IOException, TembooException{
		GetHyphenation getHyphenationChoreo = new GetHyphenation(session);

		// Get an InputSet object for the choreo
		GetHyphenationInputSet getHyphenationInputs = getHyphenationChoreo.newInputSet();

		// Set inputs
		getHyphenationInputs.set_Word(word);
		getHyphenationInputs.set_APIKey(wordnikKey);
		getHyphenationInputs.set_UseCanonical("true");

		// Execute Choreo
		GetHyphenationResultSet getHyphenationResults = getHyphenationChoreo.execute(getHyphenationInputs);
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(getHyphenationResults.get_Response());
		JsonArray rootArr = root.getAsJsonArray();
		
		if(rootArr.size() > 3){ // 4 or more syllables in a word is a polysyllabic word
			System.out.println(word + " is polysyllabic.");
			return true;
		}
		else
			return false;
	}
	
	public static void main(String [] argv) throws TembooException, URISyntaxException, IOException{
		TembooSession session = new TembooSession("gisun", "myFirstApp", "0e0ed551d2c04bae9bd5a3c9f8b58242");
		
		InitializeOAuthResultSet initializeOAuthResults = requestAuth(session);
		
		System.out.println("Processing @" + twitterUser + ". This may take up to several minutes to complete.");
		
		requestToken(session, initializeOAuthResults);
		
		JsonArray rootArr = getTweets(session);
		
		String[] tweetArray = extractTweets(rootArr);
		
		int count = 0;
		
		
		//count all polysyllabic words of the 30 tweets found.
		for(int i = 0; i < tweetArray.length; ++i){
			String[] wordArray = splitTweet(tweetArray[i]);
			for(int j = 0; j < wordArray.length; ++j){
				if(isPolysyllabic(session, wordArray[j]) == true){
					count++;
				}
			}
		}
		
		
		int grade = (int) (1.0430 * Math.sqrt(count * (30 / tweetArray.length)) + 3.1291); //SMOG formula
		
		//Output answer
		System.out.println("@" + twitterUser + "'s tweets are at grade level " + grade + ".");
	}
}
