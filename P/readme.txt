Noah K. Nam
Web and Mobile App Development
Practicum
Email: nkn28@drexel.edu


This program will parse through 30 tweets of a given user and will determine the grade level required to understand the tweets.

It will output every polysyllabic word and will output the grade level needed to read the tweets.

I programmed this in Java, using Eclipse Mars, under Windows 8. Temboo was mainly used to make API calls.

For the program to work you need the Temboo 2.11.1 SDK, and the gson SDK.
There are no comamnd line arguments.

I am assuming that the user is inputting a valid twitter handle.
I'm assuming that the README can be in .txt format as well as the extensionless format.
I am assuming that the user is OK with getting a sentence as a result over just the grade level integer.
I decided to use "4 syllables or more" instead of "3 syllables or more" as what defines a polysyllabic word. I felt that the grade levels were too inflated when I counted 3 syllable words.
I'm not too sure what is meant by "your code" in the submission guidelines, so I'll submit my Eclipse project,
along with the .java's and .class's in the root folder of A1.
I am assuming that the user has a twitter handle to provide.
I'm assuming that by "you must cite [documentation API] in your README", saying that I used Temboo is what the grader is looking for.
I'm assuming putting my code in file "P" is OK, I couldn't find what you wanted me to use as the folder name for this practicum.
I am assuming the twitter account used has at least 30 tweets.
I'm assuming duplicate words are OK

Make sure you accept the OAuth popup or the program may crash.
Don't include "@" when typing in the twitter handle.

I tested with blackboxish testing. TIME's website has similar program, and I tested my results for similarity with TIME's results, and we got the same grade levels for the twitter accounts I used.

(From A1 readme)
"Please let me know if you want my code in some other format than just the eclipse project, It was fine with Lab 1, but let me know if that was just a grace period thing, thanks!"


Wordnik temboo calls man


Citations:

https://www.temboo.com